# [TMDB Movies App](https://minghua20.gitlab.io/tmdb-movies-app/)

## Quick Start

In the project directory, you can run the app in the development mode:

```
npm install
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Search Movies
![Search_page](figures/Home_Search.jpg)

In the Search page, you can type the movie name to search. And you can also select a type (Rating, Popularity, Title) and an ordering (Ascending or Descending) to sort the results. Then, you can click the movie for a detail view like release date, language, overview, etc.

![demo1](figures/demo1.jpg)
![demo2](figures/demo2.jpg)

## Movies Gallery
![Gallery_page](figures/Home_Gallery.jpg)
In the Gallery page, you can select a genre (Sci-fi, Romance, Action, History) and view the corresponding movies.

##
This Web Application uses [TMDB](https://www.themoviedb.org/documentation/api) API.
