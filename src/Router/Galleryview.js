import axios from "axios";
import React, { useEffect, useState } from "react";
import {
    Switch,
    Route,
    Link
} from "react-router-dom";
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import GalleryList from "../components/GalleryList";
import GalleryDetails from "./GalleryDetails";


const Galleryview = () => {

    const [gmovies, setgMovies] = useState([]);
    const [genres, setGenres] = useState("");

    const getMovieRequest = async (genres) => {
    
        const response = await axios.get(`https://api.themoviedb.org/3/discover/movie?api_key=14085b5be769f41a763ed13d0e89c772&language=en-US&sort_by=popularity.desc&include_adult=false&year=2020&vote_average.gte=7&with_genres=${genres}`);
    
        const responseJson = response.data;
        setgMovies(responseJson.results);
    
    };

    useEffect(() => {
        getMovieRequest(genres);
    }, [genres]);

    return (
        <>
            <div className="gallery">
                <Link to="/Gallery" className="link" onClick={() => {setGenres("")}}>Gallery</Link>
            </div>
            <Switch>
                <Route exact path="/Gallery">
                    <div className="gallery-genres">
                        <div>
                            <Link to="/Gallery/Sci-fi" className="link">
                                <button onClick={() => {setGenres(878)}}>Sci-fi</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Romance" className="link">
                                <button onClick={() => {setGenres(10749)}}>Romance</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Action" className="link">
                                <button onClick={() => {setGenres(28)}}>Action</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/History" className="link">
                                <button onClick={() => {setGenres(36)}}>History</button>
                            </Link>
                        </div>
                    </div>
                    <div className="list-container">
                        <GalleryList genres={genres} movies={gmovies} />
                    </div>
                </Route>
            </Switch>
            <Switch>
                <Route exact path="/Gallery/Sci-fi">
                    <div className="gallery-genres">
                        <div>
                            <Link to="/Gallery/Sci-fi" className="link">
                                <button onClick={() => {setGenres(878)}}>Sci-fi</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Romance" className="link">
                                <button onClick={() => {setGenres(10749)}}>Romance</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Action" className="link">
                                <button onClick={() => {setGenres(28)}}>Action</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/History" className="link">
                                <button onClick={() => {setGenres(36)}}>History</button>
                            </Link>
                        </div>
                    </div>
                    <div className="list-container">
                        <GalleryList genres={genres} movies={gmovies} />
                    </div>
                </Route>
                <Route exact path="/Gallery/Romance">
                    <div className="gallery-genres">
                        <div>
                            <Link to="/Gallery/Sci-fi" className="link">
                                <button onClick={() => {setGenres(878)}}>Sci-fi</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Romance" className="link">
                                <button onClick={() => {setGenres(10749)}}>Romance</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Action" className="link">
                                <button onClick={() => {setGenres(28)}}>Action</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/History" className="link">
                                <button onClick={() => {setGenres(36)}}>History</button>
                            </Link>
                        </div>
                    </div>
                    <div className="list-container">
                        <GalleryList genres={genres} movies={gmovies} />
                    </div>
                </Route>
                <Route exact path="/Gallery/Action">
                    <div className="gallery-genres">
                        <div>
                            <Link to="/Gallery/Sci-fi" className="link">
                                <button onClick={() => {setGenres(878)}}>Sci-fi</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Romance" className="link">
                                <button onClick={() => {setGenres(10749)}}>Romance</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Action" className="link">
                                <button onClick={() => {setGenres(28)}}>Action</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/History" className="link">
                                <button onClick={() => {setGenres(36)}}>History</button>
                            </Link>
                        </div>
                    </div>
                    <div className="list-container">
                        <GalleryList genres={genres} movies={gmovies} />
                    </div>
                </Route>
                <Route exact path="/Gallery/History">
                    <div className="gallery-genres">
                        <div>
                            <Link to="/Gallery/Sci-fi" className="link">
                                <button onClick={() => {setGenres(878)}}>Sci-fi</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Romance" className="link">
                                <button onClick={() => {setGenres(10749)}}>Romance</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/Action" className="link">
                                <button onClick={() => {setGenres(28)}}>Action</button>
                            </Link>
                        </div>
                        <div>
                            <Link to="/Gallery/History" className="link">
                                <button onClick={() => {setGenres(36)}}>History</button>
                            </Link>
                        </div>
                    </div>
                    <div className="list-container">
                        <GalleryList genres={genres} movies={gmovies} />
                    </div>
                </Route>
            </Switch>
            <GalleryDetails genres={genres} movies={gmovies} />
        </>
    )

}

export default Galleryview;
