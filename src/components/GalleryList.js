import * as React from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import "../App.css";

const setVoteClass = (vote) => {
    if (vote >= 7.5)
        return "green";
    else if (vote >= 5 && vote < 7.5)
        return "orange";
    else
        return "red";
}

setVoteClass.propTypes = {
    vote: PropTypes.number
}

const GalleryList = (props) => {

    const {genres, movies} = props;

    var genre;
    if (genres === 878)
        genre = "/Sci-fi";
    else if (genres === 10749)
        genre = "/Romance";
    else if (genres === 28)
        genre = "/Action";
    else if (genres === 36)
        genre = "/History";
    else
        genre = "";


    return (
        <>
            {movies.map((movie) => (
                <div className="image-container">
                    <Link to={`/Gallery${genre}/${movie.id}`} className="link">
                        <img src={'https://image.tmdb.org/t/p/original' + movie.poster_path} alt={movie.original_title}></img>
                        <div className="overlay">
                            <div>{movie.title}</div>
                            <div>{movie.release_date} <span className={setVoteClass(movie.vote_average)}>{movie.vote_average}</span></div>
                        </div>
                    </Link>
                </div>
            ))}
        </>
    )
}

GalleryList.propTypes = {
    genres: PropTypes.number,
    movies: PropTypes.array
}

export default GalleryList;
