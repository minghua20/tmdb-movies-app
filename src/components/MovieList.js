import * as React from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import "../App.css";

const setVoteClass = (vote) => {
    if (vote >= 7.5)
        return "green";
    else if (vote >= 5 && vote < 7.5)
        return "orange";
    else
        return "red";
}

setVoteClass.propTypes = {
    vote: PropTypes.number
}

const MovieList = (props) => {

    const {movies} = props;

    return (
        <>
            {movies.map((movie) => (
                <div className="image-container">
                    <Link to={`/Search/${movie.id}`} className="link">
                        <img src={'https://image.tmdb.org/t/p/original' + movie.poster_path} alt={movie.original_title}></img>
                        <div className="overlay">
                            <div>{movie.title}</div>
                            <div>{movie.release_date} <span className={setVoteClass(movie.vote_average)}>{movie.vote_average}</span></div>
                        </div>
                    </Link>
                </div>
            ))} 
        </>
    )
}

MovieList.propTypes = {
    movies: PropTypes.array
}

export default MovieList;
