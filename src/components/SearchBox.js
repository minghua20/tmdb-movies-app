import React from "react";
import PropTypes from 'prop-types';
import "../App.css";

const SearchBox = (props) => {

    const {value, setSearchValue} = props;
    
    return (
        <div className="searchbox-style">
            <input value={value} onChange={(event) => setSearchValue(event.target.value)} placeholder="Type movie name to search..."></input>
        </div>
    );
};

SearchBox.propTypes = {
    value: PropTypes.string,
    setSearchValue: PropTypes.func
}

export default SearchBox;
