import React from "react";
import PropTypes from 'prop-types';
import "../App.css";

const Orderbutton = (props) => {

    const {setOrder} = props;

    var unmark = document.getElementsByClassName("check-unmarked");
    var mark = document.getElementsByClassName("check-marked");

    const click_asc = () => {
        unmark[0].style.display = "none";
        mark[0].style.display = "block";
        unmark[1].style.display = "block";
        mark[1].style.display = "none";

        setOrder('ascending');
    }

    const click_des = () => {
        unmark[1].style.display = "none";
        mark[1].style.display = "block";
        unmark[0].style.display = "block";
        mark[0].style.display = "none";

        setOrder('descending');
    }

    return (
        <>
            <div id="asc-button" className="orderbutton" onClick={() => click_asc()}>
                <div className="check-unmarked">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-square" viewBox="0 0 16 16">
                    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                    </svg> <span>Ascending</span>
                </div>
                <div className="check-marked">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-square" viewBox="0 0 16 16">
                    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                    <path d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.235.235 0 0 1 .02-.022z"/>
                    </svg> <span>Ascending</span>
                </div>
            </div>
            <div id="des-button" className="orderbutton" onClick={() => click_des()}>
                <div className="check-unmarked">
                    <svg className="check-unmarked" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-square" viewBox="0 0 16 16">
                    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                    </svg> <span>Descending</span>
                </div>
                <div className="check-marked">
                    <svg className="check-marked" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-square" viewBox="0 0 16 16">
                    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                    <path d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.235.235 0 0 1 .02-.022z"/>
                    </svg> <span>Descending</span>
                </div>
            </div>
        </>
    )
}

Orderbutton.propsTypes = {
    order: PropTypes.string,
    setOrder: PropTypes.func
}

export default Orderbutton;
