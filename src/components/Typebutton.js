import React from "react";
import PropTypes from 'prop-types';
import "../App.css";

const Typebutton = (props) => {

    const {sortType, setSortType} = props;

    return (
        <div className="typebutton">
            <select value={sortType} onChange={(event) => setSortType(event.target.value)}>
                <option disabled={true} value="">
                    -- Select a type --
                </option>
                <option value="Rating">Rating</option>
                <option value="Popularity">Popularity</option>
                <option value="Title">Title</option>
            </select>
        </div>
    )
}

Typebutton.propTypes = {
    sortType: PropTypes.string,
    setSortType: PropTypes.func
}

export default Typebutton;
