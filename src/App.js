import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import MovieList from "./components/MovieList";
import MovieListHeading from "./components/MovieListHeading";
import Orderbutton from "./components/Orderbutton";
import Initial_orderbutton from "./components/Initial_orderbutton";
import SearchBox from "./components/SearchBox";
import Typebutton from "./components/Typebutton";
import MoviesRoutes from "./Router/MoviesRoutes";
import Galleryview from "./Router/Galleryview";


const App = () => {
  const [movies, setMovies] = useState([]);
  const [searchValue, setSearchValue] = useState('');
  const [sortType, setSortType] = useState('');
  const [order, setOrder] = useState('');
  // const axios = require('axios').default;

  const getMovieRequest = async (searchValue) => {

    if (searchValue === "") {
      setMovies([]);
      setSortType([]);
      Initial_orderbutton();
      setOrder('');
    }

    const response1 = await axios.get(`https://api.themoviedb.org/3/search/movie?api_key=14085b5be769f41a763ed13d0e89c772&language=en-US&page=1&include_adult=false&query=${searchValue}`);

    const response1Json = response1.data;
    // const totalpage = response1Json.total_pages;
    var totalresponse = response1Json.results;

    // var responsei;
    // var responseiJson;
    // var resulti
    // for (let i = 2; i <= totalpage; i++) {

    //   responsei = await axios.get(`https://api.themoviedb.org/3/search/movie?api_key=14085b5be769f41a763ed13d0e89c772&language=en-US&page=${i}&include_adult=false&query=${searchValue}`);

    //   responseiJson = responsei.data;
    //   resulti = responseiJson.results;

    //   totalresponse = [...totalresponse, ...resulti];
    // }

    setMovies(totalresponse);
  };

  useEffect(() => {
    getMovieRequest(searchValue);
  }, [searchValue]);


  useEffect(() => {
    const types = {
      Rating: 'vote_average',
      Popularity: 'popularity',
      Title: 'title'
    };
    const Typevalue = types[sortType];

    if (order === 'ascending') {
      if (Typevalue === 'title') {
        const results = [...movies].sort((a, b) => a[Typevalue] > b[Typevalue] ? 1 : -1);
        setMovies(results);
      }
      else {
        const results = [...movies].sort((a, b) => a[Typevalue] - b[Typevalue]);
        setMovies(results);
      }
    }
    else {
      if (Typevalue === 'title') {
        const results = [...movies].sort((a, b) => a[Typevalue] > b[Typevalue] ? -1 : 1);
        setMovies(results);
      }
      else {
        const results = [...movies].sort((a, b) => b[Typevalue] - a[Typevalue]);
        setMovies(results);
    }
      }
    //eslint-disable-next-line
  }, [sortType, order]);


  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div className="app-background">
        <div>
          <MovieListHeading heading='Movies' />
        </div>
        <div className="search">
          <Link to="/Search" className="link">Search</Link>
        </div>
        
        <Galleryview />
        <MoviesRoutes movies={movies} />

        <Switch>
          <Route exact path="/Search">
            <div className="searchbox">
              <SearchBox searchValue={searchValue} setSearchValue={setSearchValue}/>
            </div>
            <div className="sort-order">
              <Typebutton sortType={sortType} setSortType={setSortType} />
              <Orderbutton order={order} setOrder={setOrder}/>
            </div>
            <div className="list-container">
              <MovieList movies={movies} />
            </div>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
